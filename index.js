// total number of fruits on sale
db.fruits.aggregate([{$match: {onSale:true}}, {$count: "onSale"} ]); // returns 3

// total number of fruits where stock > 20

db.fruits.aggregate([{$match: {stock:{$gt: 20}}}, {$count: "GT20"} ]); // returns 1

// average price of fruits on sale per supplier
db.fruits.aggregate([{$match: {onSale:true}}, { $group: { _id: "$supplier_id", avgPrice: { $avg: "$price" } } } ]); // supplier1: avgPrice: 20, supplier2: avgPrice: 45

// highest price of a fruit per supplier
db.fruits.aggregate([{ $group: { _id:"$supplier_id", maxPrice:{$max: "$price"} } } ]); // supplier1: maxPrice: 50, supplier2: maxPrice: 120

// lowest price of a fruit per supplier
db.fruits.aggregate([{ $group: { _id:"$supplier_id", minPrice:{$min: "$price"} } } ]); // supplier1: minPrice: 20, supplier2: minPrice: 40
